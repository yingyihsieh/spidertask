import asyncio
import aiohttp
import random
import aioredis
from bs4 import BeautifulSoup

from SpiderTask import config

class DouyuSpider:

    async def redis_execute(self,addr,li):
        redis = aioredis.from_url(addr, encoding="utf-8", decode_responses=True)
        async with redis.client() as conn:
            await conn.hset(name=li[0], mapping=li[1])

    async def crawl_async(self,url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url=url,headers={"User-Agent": random.choice(config.UA_POOL)}) as response:
                if response.status != 200:
                    raise ValueError('crawl failed')
                html = await response.text()
                soup = BeautifulSoup(html, 'html.parser')
                stream_obj = soup.find_all('li', class_='layout-Cover-item')
                for i in stream_obj[:3]:
                    info = {
                        'room_url': 'https://www.douyu.com/' + i.find('a').attrs['href'],
                        'room_image': i.find('img').attrs['src'],
                        'room_name': i.find('h3').get_text(),
                        'hot_score': i.find('span', class_='DyListCover-hot').get_text(),
                        'anchor': i.find('div', class_='DyListCover-userName').get_text()
                    }
                    print(info)
                    await self.redis_execute(config.REDIS_ROUTE, (info['room_url'],info))

    def run(self):
        tasks = [self.crawl_async(i) for i in config.Douyu_URL_LIST]
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
        asyncio.run(asyncio.wait(tasks))

if __name__ == '__main__':
    DouyuSpider().run()