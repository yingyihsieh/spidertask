# -*- coding: utf-8 -*-
import re
import time
import random
import requests
import json

from requests.cookies import RequestsCookieJar
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util import Retry
from redis import Redis
from selenium import webdriver
from selenium.webdriver.common.by import By


from SpiderTask import config

class IgSpider():
    def __init__(self):
        self.result={}
        self.graphUrl = config.INSTAGRAM_API['graph_api']
        self.path = config.Chrome_PATH
        self.api2 = ''# backdoor
        self.data = config.INSTAGRAM_URL
    def ig_token(self):
        options = webdriver.ChromeOptions()
        options.add_argument('user-agent=%s' % random.choice(config.UA_POOL))
        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        prefs = {"profile.managed_default_content_settings.images": 1}
        options.add_experimental_option("prefs", prefs)
        driver = webdriver.Chrome(executable_path=self.path,options=options)
        url = config.INSTAGRAM_API['login_url']
        driver.implicitly_wait(5)  # 隱性等待5秒
        driver.get(url)

        driver.find_element(By.XPATH, '//*[@id="loginForm"]/div/div[1]/div/label/input').send_keys(config.IG_ACCOUNT['username'])
        driver.implicitly_wait(3)
        driver.find_element(By.XPATH, '//*[@id="loginForm"]/div/div[2]/div/label/input').send_keys(config.IG_ACCOUNT['password'])
        driver.implicitly_wait(3)
        driver.find_element(By.XPATH, '//*[@id="loginForm"]/div/div[3]/button').click()
        driver.implicitly_wait(5)
        try:
            driver.find_element(By.XPATH, '//*[@id="react-root"]/section/main/div/div/div/section/div/button').click()
        except:
            pass
        time.sleep(1)
        cookie = driver.get_cookies()
        jar = RequestsCookieJar()
        for i in cookie:
            jar.set(i['name'], i['value'])
        driver.close()
        return jar

    def run(self):

        cookies = self.ig_token()
        s = requests.session()
        s.mount('https://', HTTPAdapter(max_retries=Retry(total=3, method_whitelist=frozenset(['GET', 'POST']))))

        for i in self.data:
            headers = {"User-Agent": random.choice(config.UA_POOL),}
            try:
                res = s.get(url=i, headers=headers,cookies=cookies)
                res=res.text
                pat = '"logging_page_id":"profilePage_(.*?)"'
                id = re.compile(pat, re.S).findall(res)
                if not id:
                    raise ValueError('no profile id')
                time.sleep(random.uniform(2, 4))
            except Exception as e:
                print('error',e)
                continue
            # parse video and image
            self.sendGraph(id, cookies, s)
            time.sleep(random.uniform(1.5, 5))
    def sendGraph(self,id,cookies,session):

        headers = {"User-Agent": random.choice(config.UA_POOL),
                   }
        query_hash = random.choice(config.IG_QUERY_HASH)
        variables = {
            "id": str(id[0]),
            "first": 20,
        }
        try:
            res = session.get(url=self.graphUrl % (query_hash, json.dumps(variables)), headers=headers,cookies=cookies)
            if res.status_code != 200:
                raise ValueError('graphUrl failed')
            res=res.json()['data']['user']['edge_owner_to_timeline_media']['edges']
            for j in range(len(res)):
                self.result['imgUrl']=res[j]['node']['display_resources'][0]['src']
                self.result['urlLink'] = 'https://www.instagram.com/p/%s/' % res[j]['node']['shortcode']
                self.result['title'] = res[j]['node']['edge_media_to_caption']['edges'][0]['node']['text'] if res[j]['node']['edge_media_to_caption']['edges'] != [] else '無主題'
                self.result['platform'] = 'ig'
                print(self.result)
                time.sleep(0.1)
        except Exception as e:
            print(e)

if __name__ == '__main__':
    IgSpider().run()