import time
import random
import requests

from redis import Redis

from SpiderTask import config

class TwSpider:
    def __init__(self):
        self.data = config.TWITCH_URL_LIST
        self.url = config.TWITCH_API['user_id']
        self.url2 = config.TWITCH_API['videos']
        self.result = {}
        self.self_api = ''#backdoor
        self.sr = Redis.from_url(config.REDIS_ROUTE['token'])
        self.token = self.sr.get(config.TWITCH_Client_ID)

    def refresh(self):
        """ 更新TW Token"""
        url = config.TWITCH_API['token']
        try:
            data = requests.post(url=url).json()
        except:
            raise ValueError('twitch token 異常')
        self.token = data['access_token']
        self.sr.set(config.TWITCH_Client_ID, self.token)

    def re_send(self,url):
        count = 0
        while True:
            self.refresh()
            temp = requests.get(url=url, headers=self.headers)
            if temp.status_code == 200:
                return temp
            count += 1
            if count == 3:
                return None
            time.sleep(0.5)

    def run(self):

        if not self.token:
            self.refresh()
        else:
            self.token=self.token.decode()

        self.headers = {
            'Client-ID': config.TWITCH_Client_ID,
            'Authorization': 'Bearer ' + self.token
        }

        for i in self.data:
            login=i.split('/')[-1]
            print(f'{login} start')
            try:
                temp = requests.get(url=self.url+login, headers=self.headers)
                if temp.status_code != 200:
                    temp = self.re_send(self.url+login)
                    if not temp:
                        raise ValueError('token error')
                temp = temp.json()
                time.sleep(random.uniform(0.5,2))
                resp = requests.get(url=self.url2 + temp['data'][0]['id'], headers=self.headers)

                if resp.status_code != 200:
                    resp = self.re_send(self.url2 + temp['data'][0]['id'])
                    if not resp:
                        raise ValueError('token error')
                resp=resp.json()['data']
            except Exception as e:
                print('error',e)
                continue

            for j in range(len(resp)):
                self.result['title'] = resp[j]['title']
                self.result['imgUrl'] = resp[j]['thumbnail_url'].replace('%{height}', '360').replace('%{width}', '640')
                self.result['urlLink'] = resp[j]['url']
                print(self.result)

if __name__ == '__main__':
    TwSpider().run()